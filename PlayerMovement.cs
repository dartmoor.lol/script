using UnityEngine;
using System.Collections;
using UnityEngine.InputSystem;
public class PlayerMovement : MonoBehaviour{

    [Tooltip("It is player speed")]
    public float moveSpeed;
    public Rigidbody2D rb2D;
    //public Animator animator;
    public Vector2 m_Move;
    public Controls control;
    

    public void OnShoot(InputValue value)
    {
        Debug.Log(value.Get<Vector2>());
    }

    public void OnMove(InputValue value)
    {
        
        m_Move = value.Get<Vector2>();
    }

    void Move()
    {
        Vector2 movement = new Vector2(m_Move.x, m_Move.y) * moveSpeed * Time.deltaTime;
        transform.Translate(movement);
        //rb2D.MovePosition(rb2D.position + m_Move * moveSpeed * Time.fixedDeltaTime);
        //rb2D.velocity = new Vector2(m_Move * moveSpeed * Time.fixedDeltaTime, rb2D.velocity.y);
    }
    
    void Update()
    {
        Move();
    }




   








}