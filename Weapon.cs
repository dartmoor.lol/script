using UnityEngine;
using System.Collections;

public class Weapon : ScriptableObject
{
    public string name;
    public string description;
    public int damage;
    public float shootSpeed;
    public Sprite sprite;
    public Transform firePoint;
    public GameObject bullet;
    public float bulletSpeed;
    public GameObject impactEffect;

}