using UnityEngine;
using System.Collections;

public class PlayerInfo : ScriptableObject
{
    public string name;
    public Color color;
    public Sprite sprite;
    public int coinAmount;
    public float startingHealth;
    


}