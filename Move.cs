﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class Move : MonoBehaviour
{
    public float moveSpeed;
    public Rigidbody2D rb2D;
    //public Animator animator;
    public Vector2 m_Move;
    public float RotationSpeed;
    private Quaternion lookRotation;
    private Vector3 direction;
    public Vector3 mouse_pos;

    void Update()
    {





        //create the rotation we need to be in to look at the target
        //lookRotation = Quaternion.LookRotation(mouse_pos);


        //rotate us over time according to speed until we are in the required rotation
        //transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * RotationSpeed);
        //transform.rotation = Quaternion.AngleAxis(transform.rotation, lookRotation, Time.deltaTime * RotationSpeed);
        //transform.rotation = Quaternion.LookRotation(mouse_pos * Time.deltaTime * RotationSpeed, Vector3.up);



        


        //var dir = mouse_pos - Camera.main.WorldToScreenPoint(transform.position);

        //var angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;

        //transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);


        Vector2 movement = new Vector2(m_Move.x * moveSpeed * Time.deltaTime, m_Move.y * moveSpeed * Time.deltaTime) ;
        transform.Translate(movement);

        
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame


    private void OnMove(InputValue value)
    {
        m_Move = value.Get<Vector2>();
    }


}
