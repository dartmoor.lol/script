using UnityEngine;
using System.Collections;

public class PlayerWeapon : MonoBehaviour 
{
    public Weapon weapon;
    public SpriteRenderer spriteRenderer;
    void Awake ()
    {
        
        ChangeWeapon();
    }

    void Shoot()
    {
        Instantiate(weapon.bullet, weapon.firePoint.position, weapon.firePoint.rotation);
    }

    void ChangeWeapon()
    {
        spriteRenderer = this.GetComponent<SpriteRenderer>();
        spriteRenderer.sprite = weapon.sprite;
    }
}