using UnityEngine;
using System.Collections;

public class Health : MonoBehaviour 
{

[Tooltip("It is started health")]
public int startingHealth = 100;
[Tooltip("It is current health")]
public int currentHealth;
public Animator anim;
public float delay;

public void Start()
{
    currentHealth = startingHealth;

}

public void TakeDamage (int damageAmount)
{
    currentHealth -= damageAmount;

    if(currentHealth <= 0)
    {
        Death();
    }
}

public void Death()
{
    //capsuleCollider.isTrigger = true;
    anim.SetTrigger ("Dead");
    //Destroy (gameObject,this.anim.GetCurrentAnimatorStateInfo(0).lenght + delay);
}

}
